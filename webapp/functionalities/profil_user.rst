.. _doc_webapp_profil_user:

Profil de l'utilisateur
=======================

Votre profil personnel est accessible via le menu apparaissant quand vous cliquez sur votre icône et **Votre profil**.

.. image:: img/profil/ProfileMenu.jpg
    :align: center

Vous pouvez y gérer toutes les informations vous concernant.
Vous y trouverez aussi le bouton pour changer votre mot de passe de compte.

Gestion des informations
------------------------

Ici vous pouvez visualiser et modifier (une session de moins de 5 min est requise) votre:

 * Nom
 * Avatar
 * Email
 * Délai de verrouillage
 * Mot de passe de verrouillage


.. image:: img/profil/ProfileFull.jpg

Afin de modifier des informations sensibles comme votre email ou vos mots de passe vous devez cliquer sur le bouton d’édition représenté par un crayon.

.. image:: img/profil/ProfileEditIcone.jpg
    :align: center

La validation de vos modifications se fait à l'appui du bouton vert contenant un ‘V’ qui apparaît lorsque votre modification est considérée comme valable.

.. image:: img/profil/ProfileValidIcone.jpg
    :align: center

Vérification de l’email
-----------------------

À la création de votre compte ou au changement de votre email, nous vous envoyons un mail contenant un lien afin de vérifier la validité de notre email.
Si votre email est vérifié, l’écusson sous votre avatar apparaît vert.

.. image:: img/profil/ProfileGreenBlasonOnly.jpg
    :align: center

Sinon il apparait rouge.

.. image:: img/profil/ProfileRedBlasonOnly.jpg
    :align: center

Si vous n’avez pas reçu d’email de confirmation, cliquez sur cet écusson et un nouveau mail vous sera envoyé.

Changement de mot de passe de compte
------------------------------------

Un changement de mot de passe ne peut s’effectuer que si le temps depuis la dernière connexion est **inférieur** à 5 min pour des raisons de sécurité.
Après ce délai, une **reconnexion** est nécessaire.
Afin de changer de mot de passe de connexion, il faut se rendre dans le profil personnel et dans la section **Compte**.

.. image:: img/profil/ProfileModifPWD.jpg

Un bouton **Modifier mon mot de passe** vous redirigera vers un formulaire vous demandant votre nouveau mot de passe et une confirmation.
Cliquez sur **Changer mon mot de passe** pour valider les modifications.

.. image:: img/app/AppAccountPWD.jpg

Le changement est effectif dès la prochaine connexion.