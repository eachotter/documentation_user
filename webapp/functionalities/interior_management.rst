.. _doc_webapp_interior_management:

Gestion de votre interieur
==========================

EachOtter vous permettra de créer une représentation de votre établisement afin de visualiser l'emplacement des tables.
Vous pourrez aussi les gérer et imprimer vos QR Code à tout moment.

Vos clients pourrons se situer dans votre établissement en scannant un QR Code afin d'identifier leur table.
Chaque table a un QR Code qui lui est associé. Celui-ci est généré par nos soin afin qu'il soit unique.

.. attention:: Les visuels qui suivent sont en court de modification mais leur utilisation ne devrait pas varier.

.. image:: img/interior/IntFullView.png
    :align: center

Créer une table
---------------

Pour créer une nouvelle table, cliquez sur **Créer une table**.

.. image:: img/interior/IntBtnCreateTable.jpg
    :align: center

Un formulaire vous demandant de renseigner les informations requises apparaitra.

.. image:: img/interior/IntDialogTable.jpg
    :align: center

Appuyez sur **Ajouter** pour valider la création.

.. attention:: Chaque table doit avoir un numéro **unique**.

Modifier une table
------------------

Une erreur lors de la création d'une table ? Pas de soucis, cliquez simplement sur la table dans la liste pour ouvrir une fenêtre de modification.
Appuyer sur **[Button name here]** pour enregistrer vos modifications.

.. image:: img/interior/IntModifTable.jpg
    :scale: 60%
    :align: center

Placer une table
----------------

Une fois votre table créee, elle apparaitra dans la liste de vos tables.

.. image:: img/interior/IntTableList.jpg
    :align: center

Il vous suffira alors de **glisser déposer** la table à l'emplacement qui vous convient.

.. image:: img/interior/IntDandDFix.jpg
    :scale: 70%
    :align: center

.. attention:: Pour que votre disposition soit sauvegardée, n'oubliez pas de cliquer sur **Sauvegarder** au dessus de la grille.

.. image:: img/interior/IntSave.jpg
    :align: center

.. note::   La rotation des tables arrive bientôt !

Supprimer une table
-------------------

Une icone de poubelle se trouvant à droite du nom de la table dans la liste vous permettra de supprimer une table à tout moment.

Vous pouvez aussi déplacer une table placer vers la **Corbeille** en bas a gauche pour la supprimer.

Les QR Code
-----------

Vous pouvez imprimer le QR Code d'une table spécifique depuis la fenêtre de modification de celle ci, ou bien les imprimer tous d'un coup à l'aide de **Montrer mes QR Codes**.

.. image:: img/interior/IntQRCodes.jpg
    :scale: 70%
    :align: center

Cliquez sur **Imprimer** pour lancer l'impression.
