.. _doc_webapp_bookings:

Gestion du planning
===================

EachOtter vous permettra de gérer et consulter non seulement les réservations mais aussi les événements que vous avez de planifié sur un calendrier.
Oubliez votre Google Agenda que vous devez maintenir à côté, nous vous offrons la possibilité de pouvoir gérer cela directement depuis EachOtter.

Vos clients pourront réserver une table depuis l'application mobile, il ne vous restera plus qu'à confirmer ou refuser la réservation.

.. attention:: Les visuels qui suivent sont en court de modification mais leur utilisation ne devrait pas varier.

.. image:: img/bookings/BkgFullView.png
    :align: center

.. note::   Pour le moment il est seulement possible de créer la réservation depuis la web application, l'objectif à long terme est que la réservation soit créée depuis l'application mobile du client (en cours d'implémentation).

À propos de l'interface
-----------------------

L'interface, dans cette section, est découpée en deux parties.

**- 1 - La première partie est une barre d'outils qui change en fonction du mode de visualisation du calendrier que l'on a choisie.**

.. image:: img/bookings/BkgToolbarFullView.png
    :align: center

Le calendrier est visualisable en trois mode: "Mois", "Semaine" et "Jour".

.. image:: img/bookings/BkgBtnSwitchView.png
    :align: center

La barre d'outils ou barre de navigation visible ci-dessous permet de passer d'un mode à l'autre, mais aussi de changer jour/semaine/mois selon le mode dans lequel on se trouve.

.. image:: img/bookings/BkgToolbarNav.png
    :align: center

Pour cela il suffit de cliquer sur les boutons en forme de "chevron" (gauche pour aller à la période précédente, droite pour aller à la période suivante), le bouton **Aujourd'hui** permet d'afficher la date actuel dans n'importe quel mode.

Elle permet aussi de trier les éléments affichés sur le calendrier (voir la catégorie **Trie de l'affichage**).

**- 2 - La deuxième partie est un affichage calendrier intéractif qui suit le modèle de Google Agenda.**

.. note:: Nous avons suivi le modèle de Google Agenda, car il est inscrit dans les habitudes de chacun et il permet que l'utilisateur ne soit pas trop dépaysé de ses outils habituels.

Comme vu ci-dessus, le calendrier est visualisable en trois mode: "Mois", "Semaine" et "Jour".

.. image:: img/bookings/BkgCalendarMonthView.png
    :align: center

.. image:: img/bookings/BkgCalendarWeekView.png
    :align: center

.. image:: img/bookings/BkgCalendarDayView.png
    :align: center

Les réservations/événements sont affichés dans toutes les vues.

Créer une nouvelle réservation
------------------------------

Vous voulez créer une réservation ?
Rien de plus simple, vous avez juste à **effectuer un clic** sur un jour dans le mode "Mois", ou **sélectionner une durée** dans le mode "Semaine" ou "Jour" comme ci-dessous:

.. image:: img/bookings/BkgSelectSlot.png
    :align: center

Ensuite la boîte de dialogue de création va s'afficher pour que vous puissiez créer la réservation.

.. image:: img/bookings/BkgDialogCreation.png
    :align: center

Dès les informations nécessaires remplies, vous pouvez sauvegarder les changements en appuyant sur le bouton **Créer**.

.. attention:: Il vous faut remplir tous les champs requis pour valider la création !

Consultation des détails d'une réservation
------------------------------------------

Vous pourrez consulter les détails d'une réservation en cliquant une fois sur l'événement.
Cela affichera une "popover" qui listera les détails de l'événement/réservation et permettra d'accéder à la boîte de dialogue d'édition, mais aussi à l'action de suppression.

.. image:: img/bookings/BkgPopoverDetails.png
    :align: center

.. note:: La confirmation d'une réservation arrive ! Vous pourrez depuis cette "popover" confirmer une réservation d'un client.

Modifier une réservation
------------------------

Si jamais il y a une erreur sur la réservation, pas de soucis vous pouvez l'éditer ...

Pour cela vous devez **Consulter les détails d'une réservation**, appuyez sur le bouton d'édition comme ci-dessous.

.. image:: img/bookings/BkgBtnEditBooking.png
    :align: center

Ensuite une boîte de dialogue va s'afficher afin que vous puissiez modifier les champs.

.. image:: img/bookings/BkgDialogEdit.png
    :align: center

Une fois les changements effectués, il vous suffit de valider les changements en cliquant sur le bouton **Editer**.

Supprimer une réservation
-------------------------

.. note:: La suppression d'une réservation arrive ! Vous pourrez le faire depuis la consultation des détails d'une réservation.

Tri de l'affichage
-------------------

Vous ne voulez voir que les événements ? Ne voir que les réservations ?

Vous pouvez trier l'affichage en cliquant sur les cases cochées situées dans la barre d'outils.

.. image:: img/bookings/BkgToolbarFullView.png
    :align: center

Il vous suffit de cocher pour voir et décocher pour ne plus voir les réservations/événements.
