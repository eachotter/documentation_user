.. _doc_webapp_login:

Accéder à l'application
***********************

Ecran de connexion
==================

Au lancement de l’application, l’écran de connexion s’affiche.
Utilisez les identifiants que la team EachOtter vous a communiqué pour vous connecter.
Une fois votre email et mot de passe renseignés, cliquez sur **Connexion** pour accéder à l'application.

.. image:: img/app/AppLoginScreen.jpg

Oubli de mot de passe
======================

Dans le cas d’un oubli de mot de passe, un lien *Mot de passe oublié ?* se trouvant dans l'encadré orange, vous redirigera vers une
page de demande de mot de passe. Entrez-y l’email avec lequel votre compte a été créé et cliquez sur **Réinitialiser votre mot de passe** pour recevoir un lien par mail afin de définir un nouveau mot de passe.

.. image:: img/app/AppForgotPWD.jpg

Ecran de verrouillage
=====================

L’écran de verrouillage s’affiche dès que le délai indiqué dans le profil
utilisateur est atteint ou que vous cliquez sur l'icône de verrouillage.

.. image:: img/app/AppLockIcone.jpg
    :align: center

Pour déverrouiller la session, saisissez votre code à quatre chiffres et validez à l’aide du verrou ouvert.

.. image:: img/app/AppLock.jpg

.. note::   Au bout de quatre tentatives ratées, vous serez déconnecté pour des raisons de sécurités. Votre code peut être changé plus tard via votre profil utilisateur.

Première connexion
==================

Onboarding
----------

.. attention:: Fonctionnalité en cours de développement, prévue pour septembre 2018

Après votre première connexion, les informations essentielles de votre profil vous seront demandées.
Votre nom, une image de profil ou un avatar, un nouveau mot de passe de déverrouillage.
Vous devrez renseigner ces informations avant de pouvoir continuer.
Une fois vos informations renseignées, un bouton *Terminer* vous permettra d'accéder à l'application.
