.. _doc_webapp_profil_etablissement:

Profil de l'établissement
=========================

Le profil de votre établissement est accessible via l’onglet **Votre établissement** .

.. image:: img/app/AppNav.jpg
    :align: center

.. image:: img/etablissement/EtaFullView.jpg

Vous y trouverez toutes les informations le concernant.

Gestion de la description
-------------------------

Il est possible de renseigner une description afin de donner un bref aperçu de l’esprit de votre établissement.
Entrez simplement votre texte et un bouton contenant le texte : **Modifier ma description** apparaîtra.
Il vous suffit alors de cliquer dessus pour enregistrer vos modifications.

.. image:: img/etablissement/EtaDesc.jpg

Gestion des horaires
--------------------

Le renseignement des horaires d’ouverture de votre établissement se fait via cette partie de l’application.
Vous y trouverez les sept jours de la semaine suivis d’un interrupteur signalant si votre établissement est ouvert ou
non ce jour, ainsi que deux emplacements horaires indiquant l’heure d’ouverture et de fermeture.

.. image:: img/etablissement/EtaSchedule.jpg

Ces informations seront visibles pour les consommateurs via leur application mobile.
Vous pouvez à tout moment les éditer avec un simple clic.

.. image:: img/etablissement/EtaClock.jpg
    :scale: 60%
    :align: center


Gestion des informations complémentaires
----------------------------------------

Afin de renseigner au mieux vos futurs clients, nous avons créé une section ayant pour but de vous laisser choisir quelques informations supplémentaires telles que:
* la mise à disposition de toilettes
* un accès pour personnes à mobilité réduite
* un parking

Vous pouvez aussi y indiquer les horaires de votre *Happy Hour* si vous en proposez une.
Ces informations seront accessibles par les clients sur leur application mobile.

.. image:: img/etablissement/EtaInfoComp.jpg

Localisation
------------

Afin que le plus de monde possible trouve votre établissement, nous avons un système de carte consultable par nos utilisateurs mobiles,
récapitulant tous nos partenaires. Pour y figurer, il faut remplir la section “Localisation”.
Elle se compose de 2 parties. La première étant votre addresse et la seconde votre emplacement sur la carte.

Vous y trouverez cinq champs texte:

 * Ligne d’adresse 1
 * Ligne d’adresse 2 (Facultatif)
 * Code postal
 * Ville
 * Pays

Renseignez ces 5 champs afin de nous permettre de vous localiser le plus précisément possible.

.. image:: img/etablissement/EtaAdresseForm.jpg

Un bouton **Vérifier l'adresse sur la map** apparaîtra quand l'adresse sera jugée comme valide.

.. image:: img/etablissement/EtaVerifBtn.jpg
    :align: center

Une prévisualisation de votre localisation s’affichera alors, afin que de vous permettre de vérifier
l’exactitude de l’adresse que vous avez renseignée. Cliquez **Annuler** si une erreur s’est produite ou si vous souhaitez
modifier l'adresse. Cliquer sur **C’est bien mon adresse** si la localisation vous convient.

.. image:: img/etablissement/EtaLocaMap.jpg

Une fois validée, votre établissement sera immédiatement visible sur la carte de nos utilisateurs mobiles.

Consultation des commandes archivées
------------------------------------

.. image:: img/etablissement/EtaArchivesButton.jpg
    :align: center

Pour accéder à la page de consultation des commandes archivées, cliquez sur **Voir vos commandes archivées**.

.. image:: img/etablissement/EtaArchivesView.jpg
    :align: center

Vous y trouverez un panel de recherche ainsi qu'un bouton qui permettera dans un futur proche d'exporter vos commandes.

.. attention:: Fonctionnalitées (Recherche / Export) en cours de développement, prévuees pour janvier 2019.

Afin de voir le détail de la commandes, cliquez simplement sur celle-ci.

.. image:: img/etablissement/EtaArchivesDetails.jpg
    :align: center
