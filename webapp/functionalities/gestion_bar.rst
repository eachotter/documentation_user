.. _doc_webapp_gestion_bar:

Gestion du bar
==============

EachOtter vous permettra aussi de gérer votre bar depuis votre ordinateur ou tablette sans difficulté et
d’une manière sécurisée. Votre catalogue de produits sera consultable par vos clients directement depuis leur
application mobile et ils pourront suivre les disponibilités des produits en direct.


Navigation
----------

.. image:: img/product/ProdNav.jpg

Notre navigation parmi les produits et catégories se fait en deux panels distincts.

La partie supérieure liste les différentes catégories et sous-catégories présentes.
Chaque catégorie comporte deux icônes: la première représentant un verre à cocktail indique le
nombre de produits contenus dans la catégorie et le deuxième représentant un menu indique le nombre de sous-catégories.

Afin d’entrer dans la catégorie, il vous faut cliquer sur la partie centrale.

Pour revenir à la catégorie *mère* si vous êtes dans une sous-catégorie, un bouton avec une flèche et
les labels **catégorie précédente** est présent tout à gauche.

La partie inférieure liste les produits présents dans la catégorie dans laquelle vous être actuellement.
Cliquez sur la partie centrale afin de consulter les détails d’un produit.


Gestion des produits
--------------------

Pour accéder au panel de gestion de vos produits, cliquez sur le menu **Vos produits**.

.. image:: img/app/AppNav.jpg
    :align: center

Vous arriverez ensuite sur une page vous permettant de gérer vos produits et catégories.


Création d’un nouveau produit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un bouton **+ Produit** vous permet de créer un nouveau produit.

.. image:: img/product/ProdFilterNew.jpg

Une fenêtre vous demandant les informations nécessaires à sa création s’ouvrira, vous permettant de les renseigner.

.. image:: img/product/ProdNew.jpg

Les informations demandées sont :
 * Une photo
 * Le nom du produit
 * La marque du produit
 * La catégorie
 * Une courte description du produit  ( *facultatif* )
 * Le prix Hors Taxe. ex: 5,50 donnera 5€50
 * Le prix Hors Taxe durant l'Happy Hour ( *facultatif* ). ex: 5,50 donnera 5€50
 * La T.V.A en pourcentage
 * La disponibilité du produit à l’achat

Le bouton **Annuler**, en bas à droite, vous permet de revenir au panel précédent à tout moment, mais attention, vos modifications seront **perdues**.
Une fois que vous pensez avoir renseigné toutes les informations sur votre nouveau produit, cliquez sur le bouton **Valider** afin de créer votre produit.

Modification et suppression d’un produit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour modifier un produit, il vous suffit de cliquer sur l’image du produit auquel vous voulez accéder.
Une fenêtre identique à celle que vous avez pu voir durant la création vous sera proposée. Elle contiendra les informations actuelles du produit.

.. image:: img/product/ProdModif.jpg
    :scale: 60%
    :align: center

Vous pourrez y modifier toutes les informations à votre convenance. Validez-les grâce au bouton **Mise à jour**.
Pour supprimer un produit, un bouton **Supprimer** est à votre disposition.
Une confirmation vous sera demandée avant la suppression du produit.
Cliquez sur **Oui, supprimer** pour confirmer ou **Annuler** pour revenir au panel de modification.

.. image:: img/product/ProdConfirme.jpg
    :scale: 60%
    :align: center




Gestion des catégories
----------------------

La gestion des catégories se fait au même endroit que les produits, à savoir via le menu **Vos Produits**.

.. image:: img/app/AppNav.jpg
    :align: center

Création d’une nouvelle catégorie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un bouton **+ Catégorie** vous permet de créer une nouvelle catégorie.

.. image:: img/product/ProdFilterNew.jpg

Un panel vous demandant l’intitulé de votre nouvelle catégorie et sa visibilité s’affichera.
Si vous enlevez la visibilité, la catégorie ne sera pas visible par les utilisateurs mobiles mais sera toujours consultable par vous.
Une fois le nom de votre nouvelle catégorie renseigné, cliquez sur le bouton **Valider** pour créer la catégorie ou **Annuler** pour annuler la création.

.. image:: img/product/ProdNewCate.jpg

Assigner une catégorie à un produit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour utiliser une catégorie, il suffit : soit de l’ajouter directement à la création du produit ou à la modification de celui-ci.

.. image:: img/product/ProdAddCatToProd.jpg

Une fois la catégorie assignée, cliquez sur le bouton **Mise à jour** pour enregistrer les modifications ou **Valider** pour créer le produit avec cette catégorie.

Modification et suppression d’une catégorie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour modifier la visibilité d’une catégorie, un oeil se trouve sur la *carte* représentant la catégorie en question.
Un oeil vert signifie que la catégorie est visible par les utilisateurs mobiles, un oeil rouge qu’elle ne l’est pas.
Pour changer de l’un à l’autre il vous suffit de cliquer sur l’oeil.

.. image:: img/product/ProdDetailCatVisOk.jpg
    :scale: 70%
    :align: center
.. image:: img/product/ProdDetailCatVisKo.jpg
    :scale: 70%
    :align: center

Pour modifier l’intitulé d’une catégorie, cliquez sur le nom de celle-ci pour ouvrir le panel de modification.

.. image:: img/product/ProdModifCat.jpg
    :scale: 60%
    :align: center

Faites les modifications nécessaires puis cliquez sur **Mettre à jour** pour appliquer les changements. Un bouton **Annuler** vous permet de revenir au panel de navigation.

La suppression se fait aussi via ce panel en cliquant sur le bouton **Supprimer**.
Un écran de confirmation de suppression s’affichera. Choisissez **Oui, supprimer** pour confirmer la suppression et **Annuler** pour revenir à l’écran de modification.

.. image:: img/product/ProdCateConfirme.jpg
    :scale: 60%
    :align: center

Filtrage et recherche des produits et catégories
------------------------------------------------

.. image:: img/product/ProdFilterView.jpg

Cette section vous permet de filtrer les produits ou catégories en fonction de leurs visibilités.

Choisissez ce que vous voulez filtrer avec le menu déroulant *Mode de recherche* : catégories ou produits.
Cochez **Filtrer la visibilité** pour débloquer l'interrupteur *Mode de catégories visibles*.
Par défaut, l'interrupteur sera actif, filtrant les éléments visibles.

.. image:: img/product/ProdDetailFilter.jpg

Si vous ne voulez afficher que les éléments non visibles, cliquez sur l'interrupteur pour le rendre inactif.
La flèche courbée à droite vous permet de réinitialiser votre filtrage.

Le filtrage ne marche que pour les éléments actuellement affichés à votre écran.


Un mode de filtrage global est en cours. Il permettra, par exemple,
d’afficher tous les produits non visibles quelles que soient leurs catégories ou sous-catégories.

.. note:: Une recherche par mot-clés est en cours de développement afin de pouvoir rechercher un produit ou une catégorie par son nom.


Gestion des commandes
---------------------
.. attention:: Fonctionnalité en cours de développement, prévue pour septembre 2018.

Pour accéder au panel de gestion des commandes, il vous faut cliquer sur le menu **Commandes**.

.. image:: img/app/AppNav.jpg
    :align: center

Organisation générale
~~~~~~~~~~~~~~~~~~~~~

L’écran sur lequel vous arriverez comporte trois colonnes.

.. image:: img/order/OrderGlobView.jpg

La colonne la plus à gauche est intitulée **A Faire** et contient les commandes reçues par les applications mobiles qui n’ont pas été commencées.

La colonne du centre **En Préparation** contient les commandes qui sont en cours de préparation.

La colonne la plus à droite **Préparées** contient les commandes en attente de service à table ou de retrait par le consommateur.

Les commandes sont présentées avec leur numéro précédé d’un **#** .

.. note:: Le numéro de la table ou de l’utilisateur de l’application mobile ayant passé commande sera noté en dessous.


Consultation des commandes et actions possibles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour accéder au contenu de la commande, cliquez sur le chevron à droite du numéro de commande.
La carte s’ouvrira, faisant apparaître dans la première colonne la quantité et dans la deuxième le produit.

.. image:: img/order/OrderExpanded.jpg
    :scale: 70%
    :align: center

Pour plus de détails, un bouton **Détails** se trouve sur le bas de la carte ouverte.

.. image:: img/order/OrderDetail.jpg

Un récapitulatif détaillé de la commande apparaîtra stipulant les quantités, les produits,
les prix Hors Taxes, les taux de T.V.A, les prix Toute Taxe Comprise et le total de la commande.

Si pour quelque raison il vous faut annuler une commande, le bouton **Annuler** vous mènera
vers une confirmation d’annulation. Cliquez sur “Annuler la commande” pour confirmer. Notez que l’annulation
d’une commande procède au **remboursement** total du client. Une annulation d’un produit en particulier d’une
commande et un remboursement partiel n’est pas possible.

.. image:: img/order/OrderConfCancel.jpg

Pour faire passer une commande d’une colonne à l’autre, un simple **glissement** de la
commande du bout du doigt **vers la colonne visée** suffit. Une notification sera envoyée au client à chaque changement d’état de sa commande.

Le changement ne se fait que vers la droite. Un changement de la colonne **Préparées** à **En Préparation** n’est pas possible.

Une fois la commande servie ou récupérée par le client, un glissement vers l'extrémité droite de l’écran archivera la commande.

Consultation des commandes archivées
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La consultation des commandes archivées ce fait fait via le **Profile de l'établissement**.




