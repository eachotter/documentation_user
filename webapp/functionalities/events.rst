.. _doc_webapp_events:

Gestion de vos événements
=========================

EachOtter vous permettra de gérer et promouvoir auprès de vos clients tous vos événements.
Ils pourront les voir en direct depuis leur application mobile en cliquant sur les détails de votre bar.

Vos clients pourront profiter d'informations inédites sur les événements que vous proposerez et publierez sur la plateforme.
Ces événements seront à terme visible dans la partie :doc:`Gestion des réservations<bookings>`.

.. attention:: Les visuels qui suivent sont en court de modification mais leur utilisation ne devrait pas varier.

.. image:: img/events/EvtFullView.png
    :align: center

Créer un nouvel événement
-------------------------

Pour créer une nouvelle évenement, cliquez sur **Créer un événement**.

.. image:: img/events/EvtBtnCreateEvent.png
    :align: center

Un formulaire vous demandant de renseigner les informations requises apparaitra.

.. image:: img/events/EvtDialogEvent.png
    :align: center

Appuyez sur **Créer** pour valider la création.

.. attention:: Si vous ne précisez pas que l'événement est payant le prix ne sera pas affiché. Pour cela il faut cliquer sur le bouton **Payant**.

Consulter les détails
---------------------

Une fois votre événement créé, vous avez la possibilité consulter ses détails.
Que ce soit pour vérifier si vous n'avez pas fait d'erreurs, ou encore y revenir plus tard si vous voulez le supprimer ou le modifier.
Pour cela cliquez simplement sur la flêche en bas à droite de l'événement.

.. image:: img/events/EvtBtnExpand.png
    :align: center

Les détails de votre événement vont donc apparaître comme ceci :

.. image:: img/events/EvtExpandedEvent.png
    :align: center

Deux actions s'offrent donc à vous **Modifier un événement** ou **Supprimer un événement**

Modifier un événement
---------------------

Une erreur lors de la création de votre événement ? Pas de soucis, vous n'avez qu`à **Consulter les détails** de l'événement.

.. image:: img/events/EvtExpandedEvent.png
    :align: center

Il vous suffira de cliquer sur le bouton **Edit** pour modifier votre événement, une boîte de dialogue s'ouvrira à cet effet.

.. image:: img/events/EvtEditDialogEvent.png
    :align: center

.. attention:: Pour que vos changements soient sauvegardés, n'oubliez pas de cliquer sur **Mettre à jour** en bas à droite de la boîte de dialogue.

.. image:: img/events/EvtBtnUpdate.png
    :align: center

Supprimer un événement
----------------------

À nouveau lorsque vous **Consulter les détails** de l'événement vous pouvez supprimer un événement.

.. image:: img/events/EvtExpandedEvent.png
    :align: center

Cliquez sur le bouton **Delete** afin de faire disparaître à tout jamais l'événement de votre choix.

Trier les événements
--------------------

À droite du bouton de création, vous pourrez constater un bouton servant au tri de vos événements.

.. image:: img/events/EvtExpandedEvent.png
    :align: center

Lorsque vous cliquer dessus, vous aurez la possibilité de trier vos événements selon plusieurs critères.

.. image:: img/events/EvtBtnTrierDropdown.png
    :align: center
