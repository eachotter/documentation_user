***************
Fonctionnalités
***************

.. toctree::
   :maxdepth: 1
   :name: toc-functionalities

   login
   profil_user
   profil_etablissement
   gestion_bar
   interior_management
   events
   bookings
   music

.. history
.. authors
.. license
