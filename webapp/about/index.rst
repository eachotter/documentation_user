A propos de la Web Application
==============================

Notre web application est destinée avant tout aux professionnels travaillant dans des établissements proposant à leurs clients la consommation de boissons ou autres denrées.

EachOtter vous permet de gérer votre établissement (commandes, carte, événements, réservations...) du bout des doigts, tout en faisant de lui un catalyseur de communication entre vos clients possédant l'application mobile.

Ce document vous aidera à utiliser notre solution de la manière la plus optimale possible.

.. history
.. authors
.. license
