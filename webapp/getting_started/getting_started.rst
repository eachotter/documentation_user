.. _doc_webapp_getting_stated:

Installation
============

Pour installer EachOtter sur votre ordinateur il faut tout d’abord télécharger
l’installateur correspondant à votre système d’exploitation (Windows, MacOS).

Installation Windows
--------------------

Lancez le fichier que vous avez téléchargé : EachOtter Setup v1.exe.

.. image:: img/EOExe.JPG
    :align: center

Une fenêtre d’installation s’ouvrira et installera directement l'application.

.. image:: img/EOSetup.jpg
    :align: center

Une fois celle-ci installée, l'application se lancera directement.

.. image:: img/EOLaunch.jpg
    :align: center

Une icone EachOtter se trouvera sur votre bureau pour lancer l’application à tout moment.

Installation MacOS
------------------

.. attention:: Fonctionnalité en cours de développement


Mise à Jour
===========

.. attention:: Fonctionnalité en cours de développement


Désinstallation
===============

Pour le moment, la désinstallation de l'application se fait via le **panneau de configuration** -> **désinstallation d'un programme**.
Sélectionnez EachOtter dans la liste et désinstallez la de la même manière que les autres programmes.

.. image:: img/EOUninstall.jpg
    :align: center
