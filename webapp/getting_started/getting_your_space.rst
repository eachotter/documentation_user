.. _doc_webapp_getting_your_space:

Demander son OtterSpace
=======================

Voici la démarche pour pouvoir utiliser EachOtter dans votre établissement. Ou bien **Comment obtenir votre OtterSpace**.

Rien de plus simple! Rendez vous sur notre site web : https://www.eachotter.com/ et vous trouverez en bas de page un bouton vous permettant de faire une demande de beta.

.. image:: img/EOBetaBtn.jpg
    :align: center

Remplissez le formulaire et nous reviendrons vers vous dès que votre OtterSpace sera prêt. N'hésitez pas à nous contacter par mail pour plus d'informations.

.. image:: img/EOBetaForm.jpg
    :align: center
