# -*- coding: utf-8 -*-
#
# EachOtter User documentation build configuration file

import sys
import os
from sphinx.builders.html import StandaloneHTMLBuilder
StandaloneHTMLBuilder.supported_image_types = ['image/png', 'image/svg+xml',
                                               'image/gif', 'image/jpeg']


# -- General configuration ------------------------------------------------

needs_sphinx = '1.6'

# Sphinx extension module names and templates location
sys.path.append(os.path.abspath('extensions'))
extensions = ['gdscript', 'sphinx.ext.imgmath']
templates_path = ['_templates']

# You can specify multiple suffix as a list of string: ['.rst', '.md']
source_suffix = '.rst'
source_encoding = 'utf-8-sig'

# The master toctree document
master_doc = 'index'

# General information about the project
project = 'EachOtter'
copyright = '2017-2018, EachOtter'
author = 'Tiphaine, Jonathan, Kylian, Loic, Terrence, Valentin'

# Version info for the project, acts as replacement for |version| and |release|
# The short X.Y version
version = 'latest'
# The full version, including alpha/beta/rc tags
release = 'latest'

language = 'fr'

exclude_patterns = ['_build']

from gdscript import GDScriptLexer
from sphinx.highlighting import lexers
lexers['gdscript'] = GDScriptLexer()

# Pygments (syntax highlighting) style to use
pygments_style = 'sphinx'
highlight_language = 'gdscript'

# -- Options for HTML output ----------------------------------------------

# on_rtd is whether we are on readthedocs.org, this line of code grabbed from docs.readthedocs.org
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

import sphinx_rtd_theme
html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
if on_rtd:
	using_rtd_theme = True

# Theme options
html_theme_options = {
    # 'typekit_id': 'hiw1hhg',
    # 'analytics_id': '',
    # 'sticky_navigation': True  # Set to False to disable the sticky nav while scrolling.
    'logo_only': True,  # if we have a html_logo below, this shows /only/ the logo with no title text
    'collapse_navigation': False,  # Collapse navigation (False makes it tree-like)
    # 'display_version': True,  # Display the docs version
    # 'navigation_depth': 4,  # Depth of the headers shown in the navigation bar
}

# VCS options: https://docs.readthedocs.io/en/latest/vcs.html#github
html_context = {
    "display_gitlab": True, # Integrate GitLab
    "gitlab_user": "eachotter", # Username
    "gitlab_repo": "documentation_user", # Repo name
    "gitlab_version": "master", # Version
    "conf_py_path": "/", # Path in the checkout to the docs root
}

html_logo = 'img/docs_logo.png'

# Output file base name for HTML help builder
htmlhelp_basename = 'EachOtterUserDoc'

# -- Options for reStructuredText parser ----------------------------------

# Enable directives that insert the contents of external files
file_insertion_enabled = False

# -- Options for LaTeX output ---------------------------------------------

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).

latex_engine = 'pdflatex'
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    'papersize': 'a4paper',
    'releasename': " ",
    # Sonny, Lenny, Glenn, Conny, Rejne, Bjarne and Bjornstrup
    # 'fncychap': '\\usepackage[Lenny]{fncychap}',
    'fncychap': '\\usepackage{fncychap}',
    'fontpkg': '\\usepackage{amsmath,amsfonts,amssymb,amsthm}',

    'figure_align': 'htbp',
    # The font size ('10pt', '11pt' or '12pt').
    #
    'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    'preamble': r'''
        \usepackage[utf8]{inputenc}
        \usepackage{fullpage}
        \usepackage{graphicx}
        \usepackage{fancyhdr}	% headers/footers
        \usepackage{xcolor}		% to use our own color
        \usepackage{lastpage}	% to easily know the total number of pages
        \usepackage{titling}	% to easily know the total number of pages
        \usepackage{colortbl}	% to put color in a table background
        \usepackage{datetime}	% to allow us set a new date formatting
        \usepackage{multirow}   % to allow multirows in tables
        \usepackage{hyperref}
        \usepackage{tikz}
        \usepackage{longtable}
        \usepackage{textcomp}
        \usepackage{lmodern}
        \usetikzlibrary{arrows,shapes,positioning,shadows,trees}
        

        \usepackage{titlesec, blindtext, color}
        \definecolor{gray75}{gray}{0.75}
        \newcommand{\hsp}{\hspace{20pt}}
        \titleformat{\chapter}[hang]{\Huge\bfseries}{\thechapter\hsp\textcolor{gray75}{|}\hsp}{0pt}{\Huge\bfseries}
        
        \let\cleardoublepage\clearpage
        
        %%%add number to subsubsection 2=subsection, 3=subsubsection
        %%% below subsubsection is not good idea.
        \setcounter{secnumdepth}{3}
        %
        %%%% Table of content upto 2=subsection, 3=subsubsection
        \setcounter{tocdepth}{2}
        
        % Custom defines zone
        \setlength{\headsep}{10pt}
        
        % Define useful hand-made colors
        \definecolor{epiBlue}{RGB}{23,54,93}
        \definecolor{lightGray}{gray}{0.85}
        
        % Bit of code to bold an entire table row
        % http://tex.stackexchange.com/questions/4811/make-first-row-of-table-all-bold
        \newcolumntype{$}{>{\global\let\currentrowstyle\relax}}
        \newcolumntype{^}{>{\currentrowstyle}}
        \newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}%
          #1\ignorespaces
        }
       
        
        % Define Document Title
        \input{constant.tex}
        
        % end of Defines
        
        
        % fancyhdr-specific commands
        \setlength{\headheight}{15.2pt}
        
        %% Defining headers and footers contents.
        
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrulewidth}{1pt}
        
        \fancypagestyle{empty}
        {
            \fancyhead[L]{\includegraphics[height=42pt]{logo_eip.png}
            }
            \fancyhead[C]{\includegraphics[height=42pt]{titre.png}
            }
            \fancyhead[R]{\includegraphics[height=42pt]{logo.png}
            }
            
        
            \fancyfoot[L]{
                \textcolor{gray}{\Epitech}
            }
            \fancyfoot[C]{
                \textcolor{black}{\DocPath}
            }
            \fancyfoot[R]{
                \thepage/\pageref{LastPage}
            }
        }
        
        \fancypagestyle{EIP}
        {
            \fancyhead[L]{\includegraphics[height=42pt]{logo_eip.png}
            }
            \fancyhead[C]{\includegraphics[height=42pt]{titre.png}
            }
            \fancyhead[R]{\includegraphics[height=42pt]{logo.png}
            }
        
            \fancyfoot[L]{
                \textcolor{gray}{\Epitech}
            }
            \fancyfoot[C]{
                \textcolor{black}{\DocPath}
            }
            \fancyfoot[R]{
                \thepage/\pageref{LastPage}
            }
            
        }
        
        \fancypagestyle{plain}{%
            \fancyhf{}%
            \fancyfoot[L]{
                \textcolor{gray}{\Epitech}
            }
            \fancyfoot[C]{
                \textcolor{black}{\DocPath}
            }
            \fancyfoot[R]{
                \thepage/\pageref{LastPage}
            }
           
        }
        
    ''',


    'maketitle': r'''
    
        \pagestyle{empty}
        \pagenumbering{arabic}
                
        \input{cover.tex}
        
        
        
        \input{resume.tex}
        
        \input{description_document.tex}
        
        \pagestyle{EIP}

        \renewcommand{\contentsname}{Table des Matières}
        
        \tableofcontents
        \clearpage
        

        ''',
    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
    # 'sphinxsetup': \
    #     'hmargin={0.7in,0.7in}, vmargin={1in,1in}, \
    #     verbatimwithframe=true, \
    #     TitleColor={rgb}{0,0,0}, \
    #     HeaderFamily=\\rmfamily\\bfseries, \
    #     InnerLinkColor={rgb}{0,0,1}, \
    #     OuterLinkColor={rgb}{0,0,1}',
    #
    'tableofcontents': ' ',

}

latex_logo = '_static/img/logo.png'
latex_additional_files = ['_static/img/logo_eip.png',
                          '_static/img/titre.png',
                          '_static/pages/resume.tex',
                          '_static/constant.tex',
                          '_static/pages/cover.tex',
                          '_static/pages/description_document.tex']

latex_documents = [
    (master_doc, '2019_UD2_eachotter.tex', 'EachOtter User Documentation',
     author, 'manual'),
]


# -- Options for linkcheck builder ----------------------------------------

# disable checking urls with about.html#this_part_of_page anchors
linkcheck_anchors = False

linkcheck_timeout = 10

# -- I18n settings --------------------------------------------------------

locale_dirs = ['../sphinx/po/']
gettext_compact = False
# Exclude class reference when marked with tag i18n.
# if tags.has('i18n'):
#     exclude_patterns = ['classes']
