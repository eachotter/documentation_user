A propos de EachOtter
=====================

Bienvenue sur la documentation utilisateur officielle de l'application EachOtter.

EachOtter est une solution se déclinant en deux applications distinctes.

Notre objectif est de proposer de la visibilité et de faciliter la prise de commandes pour nos partenaires, tout en permettant à leurs clients d'intérargir avec l'établissement (vote de musique, réservation, proposition d'événements...).
Pour cela nous sommes en train de développer deux applications bien distinctes.
Pour nos partenaires professionnels, une web application leur permettant de gérer leur établissement du bout des doigts.
Pour les particuliers, une application mobile leur permettant de voir nos partenaires proches d'eux, de commander une fois dans leur enceinte, mais aussi de communiquer avec les autres utilisateurs proches de lui.

Voici un schemas global représentant la manière dont nous avons imaginé les interactions de nos applications.

.. image:: ../img/GraphGlobal.jpg

EachOtter est un Epitech Innovative Project.



.. history
.. authors
.. license
