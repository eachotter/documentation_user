Fonctionnalités
===============

.. toctree::
   :maxdepth: 1
   :name: toc-functionalities

   login
   profil
   find_place
   orders
   communicate
   faq

.. history
.. authors
.. license
