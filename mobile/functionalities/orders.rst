.. _doc_mobile_orders:

Commander
=========

Commander des produits
----------------------

.. attention:: Le visuel est en cours de modification

.. image:: img/MobileOrderList.png
    :scale: 65%
    :align: center

Cette vue permet de lister tous les produits consommables dans le bar dans lequel vous êtes actuellement.

Le bouton (1) situé en bas à droite de chaque produit sert à l’ajouter dans votre panier afin de pouvoir le commander par la suite.


Le Panier
---------

.. attention:: Le visuel est en cours de modification

.. image:: img/MobilePanier.png
    :scale: 65%
    :align: center

Sur cette vue, vous trouverez tous les produits que vous avez ajoutés dans votre panier.

Le plus (1), situé en bas et à droite de chaque article, vous permettra d’ajouter une unité du produit en question dans votre panier.

Le moins (2) vous servira à diminuer la quantité du produit en question.

Le coût total (3) de votre panier est affiché en bas à gauche.

En appuyant sur le bouton Valider la commande (4), vous pourrez choisir votre carte à utiliser pour payer le montant de votre panier afin de lancer la commande.


Payer votre commande
--------------------

Le paiement des commandes sera intégré à l’application en utilisant Stripe.

L’utilisateur pourra alors enregistrer une carte de paiement et s’en servir dans le cadre de ses achats.

Pour ce faire, il suffit d'aller sur la page "Méthodes de paiement".

.. image:: img/AddCard.png
    :scale: 65%
    :align: center

Sur cette vue, vous pouvez retrouvez un récapitulatif des cartes que vous avez liées à votre compte, et vous avez également la possibilité d'en ajouter de nouvelles
en cliquant sur "Ajouter une carte bancaire".

.. image:: img/AddNewCart.png
    :scale: 65%
    :align: center

La première information à remplir est votre numéro de carte bancaire (1).

Ensuite, vous devrez renseigner la date d'expiration (2) ainsi que le cryptogramme visuel (3) présent au dos de votre carte bancaire.

Si vous souhaitez annuler l'opération, vous pouvez revenir en arrière en cliquant sur "Retour"(4).

Enfin, si vous souhaitez valider l'enregistrement de votre carte, cliquez sur "Enregistrer"(5).
