.. _doc_mobile_faq:


Foire Aux Questions
===================

Je ne trouve pas le bar dans lequel je vais d’habitude, pourquoi ?
------------------------------------------------------------------

Il se peut que votre bar favori ne soit pas encore partenaire avec nous !
Ne perdez pas une seconde de plus et n’hésitez à lui parler de nous et d’expliquer pourquoi ils devraient faire appel à nous !


Un produit de la carte n’est pas présent lorsque j’essaie de le commander
-------------------------------------------------------------------------
Si un bar propose un produit et que vous ne le trouvez lors de votre commande, il se peut que ce produit ne soit plus en stock dans le bar.
L’équipe du bar a la possibilité de gérer les produits qu’il veut afficher ou non au sein de la carte.


Je ne peux pas me connecter
---------------------------

Si vous avez des problèmes de connexion, cela peut être seulement temporaire et nous vous prions de bien vouloir réessayer ultérieurement.
Malgré cela si le problème persiste, voici quelques astuces à essayer ! Mais avant cela vérifiez que vous soyez sur la dernière version de EachOtter.

Sur iPhone :

Veuillez fermer entièrement l’application : double-cliquez sur le bouton Accueil et faites glisser l'application EachOtter vers le haut et
essayez de l’ouvrir à nouveau. Assurez-vous que vous vous connectez avec le bon compte Facebook ou Google, et vérifiez les réglages iOS de votre téléphone.

Sur Android :

Veuillez fermer entièrement l’application : allez dans « Réglages », trouvez « Apps », sélectionnez EachOtter et
cliquez sur « Forcer l’arrêt » puis ouvrez à nouveau l’application. Si le problème est toujours présent,
Dans le menu « Apps » d’EachOtter, choissiez « Stockage » puis appuyez sur « Effacer les données ».
Si aucune de ces solutions ne fonctionnent, veuillez contacter le support de l’application.


Pourquoi ma photo/description a-t-elle été supprimée de mon profil ?
--------------------------------------------------------------------

Nous devons parfois supprimer des photos ou des descriptions de EachOtter pour garantir une expérience agréable pour tout le monde.
Si vous êtes concerné pour ce problème, cela signifie qu’elle ne respectait pas nos Conditions d’utilisation.


Un ou des messages ont disparu
------------------------------

Si une conversation avec une personne a disparu, cela signifie que la personne n’a pas voulu continuer
cette conversation avec vous ou que cette personne a supprimé son compte.

Vous pouvez vous déconnecter et vous reconnecter de EachOtter pour en être sûr.


Quelqu’un utilise une photo de moi ou se fait passer pour moi
-------------------------------------------------------------

En vertu du droit à l'image, chacun peut contrôler l'utilisation de son identité dans le cadre de la publicité,
comme lorsque l'image d'une célébrité est utilisée pour sponsoriser un produit.

Si votre nom ou une photo/vidéo de vous n'est pas utilisé dans une publicité, mais que celui-ci
apparaît dans le contenu de quelqu'un d'autre sur EachOtter d'une manière que vous estimez représenter un cas d'abus,
de harcèlement ou d'intimidation, signalez-le nous.

Si quelqu'un utilise votre nom ou une photo/vidéo de vous sans votre autorisation dans une publicité,
n'hésitez pas à nous le faire savoir.


Les gages des mini-jeux m’engagent-ils en quoi que ce soit ?
------------------------------------------------------------

Bien évidemment, les gages présents dans les mini-jeux ne sont là que pour les
rendre plus ludiques, en aucun cas vous n’êtes obligé de les respecter quoiqu’il advienne.


J’ai un problème avec le paiement de ma commande
------------------------------------------------

Pour tout problème lié au paiement d’une commande, veuillez consulter l’assistance de Stripe.

Puis-je effacer mes données personnelles présentent sur EachOtter ?
-------------------------------------------------------------------

Bien évidemment ! De plus, toute entreprise stockant des données personnelles se doivent de le faire depuis la loi GDPR mise en vigueur le 26 Mai 2018.
Si vous voulez consulter ou supprimer vos informations personnelles, n'hésitez pas a nous contacter contact@eachotter.com !
