.. _doc_mobile_profil:

Votre Profil
============

La page de profil vous permet d’avoir une vision d’ensemble sur vous au sein de l’application et vous permet également d’accéder à vos messages et contacts.

.. image:: img/MobileProfile.png
    :scale: 65%
    :align: center

Sur votre profil, vous retrouverez votre photo de profil(1) modifiable à tout moment dans les Options(12).


Vous disposez également un Badge(2) et de Points(3) représentant vos exploits effectués sur l’application.
En plus de votre pseudo(5), vous disposez également d’un titre(4) qui s’obtiendra également en fonction de
vos exploits réalisés sur l’application et d'un tag(7) pour permettre de a plusieurs utilisateurs d'avoir le même pseudo. Une description(6) de vous est également présente et modifiable dans les Options(9).

L'icône (8) vous permet de scanner ou d'afficher un QR Code, QRCode permettant d'ajouter un ami ou d'être ajouter en ami par un autre utilisateur.
Vous pouvez voir tout vos exploits grâce à la page des Trophés(9).

Vous retrouverez vos réservations via le bouton "Réservations"(10)
L'icône (11) sert à acceder à la messagerie.

La dernière icône(12) permet d’accéder aux options de votre profil. Via cette page vous pourrez modifier
plusieurs informations tel que votre pseudo, votre photo de profil ou votre mot de passe.


Modifier votre Profil
---------------------

Sur la page des options de votre profil, vous pourrez modifier plusieurs informations:

.. image:: img/MobileModif.png
    :scale: 65%
    :align: center

La première chose que vous pouvez modifier sur cette page est votre titre(1).
Une fois que vous aurez appuyé sur le titre actif, une liste déroulante s’affichera avec tous vos titres
disponibles et vous n’aurez qu'à choisir celui qui vous convient le mieux.

Vous pourrez également modifier votre description(2).

Si vous voulez changer d’adresse mail, vous devrez renseigner la nouvelle adresse dans les cases (3) et (4).

Vous pouvez également modifier votre mot de passe avec la même méthode que pour changer votre adresse mail.
Il suffit de renseigner votre nouveau mot de passe dans les cases (5) et (6).

Si vous souhaitez changer votre photo de profil, il suffit de cliquer sur le bouton **Change Picture** (7),
une nouvelle fenêtre s'ouvrira alors il vous suffira soit de choisir une photo dans votre téléphone soit de
prendre une photo. Une fois la photo sélectionnée, vous aurez la possibilité de choisir la zone que vous voulez garder.

Pour enregistrer vos modifications, il vous suffit de cliquer sur le bouton **Submit** (8) ou si vous souhaitez annuler
vos modifications, il vous suffit de cliquer sur le bouton **Cancel** (9).

.. image:: img/MobilePicture.png
    :scale: 65%
    :align: center

Le rectangle(1) blanc vous permet de choisir la zone que vous souhaitez garder.
Le curseur(2) quant à lui vous permet de régler la taille de l’image.

Vous pouvez choisir de modifier la taille de l’image(3) ou alors d’effectuer une rotation sur celle-ci(4).

Si vous souhaitez annuler le changement de votre image de profil, vous pouvez appuyer sur la croix(5)
et si vous avez terminé vos modifications, vous pouvez valider la nouvelle photo en appuyant sur le bouton de validation(6).
