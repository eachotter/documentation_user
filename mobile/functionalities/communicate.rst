.. _doc_mobile_communicate:

Communiquer
===========

La Messagerie
-------------

.. image:: img/chat.png
    :scale: 65%
    :align: center

Cette partie de l’application vous permettra de communiquer avec vos contacts et/ou d’autres personnes présentes dans le bar.
Sur l'image ci-dessus, vous retrouverez une discussions entre deux personnes.

Vous avez les messages échangés entre les deux personnes (1). Mais si vous souhaitez envoyer un message, il vous suffit d'ouvrir le clavier en appuyant sur la zone d'écriture (2).



Les Contacts
------------

.. attention:: Fonctionnalité en cours de développement, prévue pour janvier 2019

Cette vue listera tous contacts afin de pouvoir converser avec eux, accéder à leur profil ou bien les supprimer.


La Fiche contact
----------------

.. attention:: Fonctionnalité en cours de développement, prévue pour janvier 2019

Vous pourrez y accéder depuis la vue précédente (contacts) afin d’obtenir les différentes informations
du contact en question (username, photo, description, …).
