.. _doc_mobile_find_place:

Trouver un Lieu
===============

La Carte
--------

.. attention:: Le visuel est en cours de modification

Une fois connecté à l’application, vous serez redirigé vers la Carte,
c’est une carte affichant les bars EachOtter autour de vous.

.. image:: img/MobileMap.png
    :scale: 30%
    :align: center

La punaise(1) indique un bar qu’un bar EachOtter est présent. En appuyant sur cette punaise,
vous aurez accès au nom du bar ainsi qu’une brève description.
Cela déclenchera également l’apparition d’une icône(2) vous permettant d’aller sur
la page du bar ce qui vous permettra notamment de commander dans le lieu.

La barre de recherche(3) vous permet de chercher un bar par son nom.

Vous pouvez également avoir un itinéraire de votre emplacement vers le bar avec le bouton d’itinéraire(4).
Attention, cette action vous lancera votre application GPS par défaut (Google Maps pour Android et Plan pour iOS)

Le bouton **Log out** (5) vous permet de vous déconnecter de votre compte.

Le bouton (6) est le bouton actif par défaut. C’est celui qui vous affiche la BarMap.
Le bouton (7) quand à lui, vous redirige vers votre profil.


Description du lieu
-------------------

.. attention:: Le visuel est en cours de modification

.. image:: img/MobileEtabView.png
    :scale: 75%
    :align: center

Sur cette vue vous trouverez toutes les informations liées à l'établissement partenaire d’EachOtter.

Vous y trouverez son adresse, les moyens de le contacter ainsi que les événements (à venir et passés) se déroulant là-bas.

La barre de navigation (1) vous permettra d'accéder à la carte du bar. Le bouton "Réserver" (2)
permet de réserver une table dans l'établissement concerné et le cadre en dessous (3)
décrit les évènements de ce bar.
