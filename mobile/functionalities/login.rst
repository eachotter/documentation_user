.. _doc_mobile_login:

Acceder à l'application
=======================

Premier lancement
-----------------

.. image:: img/MobileLaunchScreen.jpg
    :scale: 60%
    :align: center

Au premier lancement de l’application, cette vue s’affichera.

Si vous possédez déjà un identifiant, cliquez sur le bouton **Log In** (1).

Pour vous inscrire,utilisez le bouton **Sign Up** (2) pour vous inscrire.

Enfin, pour vous identifier en utilisant Facebook ou Google, cliquez sur les boutons concernés (3 et 4) et suivez la procédure indiquée.

Connexion classique
-------------------
.. image:: img/MobileClassicCo.jpg
    :scale: 60%
    :align: center

Si vous avez choisi la connexion classique, renseignez votre email (1) ainsi que votre mot de passe (2) puis utilisez le bouton **Sign In** (4).

En cas d’oubli de votre mot de passe, le bouton **Forgot password ?**  (3) est là pour vous.

Enfin, la flèche (5) en haut et à gauche vous permettra de revenir sur la vue précédente en cas d’erreur de votre part.

Mot de passe oublié
-------------------

.. image:: img/MobilePWDForg.jpg
    :scale: 60%
    :align: center

Dans le cas d’un oubli de mot de passe, renseignez votre adresse email (1) puis cliquez sur le bouton **Send email** (2) pour recevoir votre nouveau mot de passe par email.

Comme précédemment, une flèche (3) vous permettra de revenir en arrière.

Inscription
-----------

Pour accéder à toutes les fonctionnalités de l’application EachOtter, vous aurez besoin de vous inscrire. Pour se faire, vous devrez remplir le formulaire ci-dessous:

.. image:: img/MobileBlackVersion.png
    :scale: 30%
    :align: center
    :alt: inscription

Dans la case Username(1), vous devrez renseigner le nom avec lequel vous voulez apparaître pour les autres utilisateurs.


En dessous, vous devrez renseigner votre adresse email(2), cette adresse email vous servira d’identifiant de connexion et pour changer votre mot de passe. Vous devrez ensuite confirmer votre adresse email dans le champ (3).

Dans le quatrième champ de saisie, vous devrez renseigner un mot de passe(4). Ce mot de passe vous sera demandé avec votre adresse email pour pouvoir accéder à votre compte. Une fois ce champ rempli vous devrez renseigner le même mot de passe en dessous(5).

Une fois que vous avez rempli tous les champs, cliquez sur le bouton “Sign Up”(6). Si vous souhaitez annuler votre inscription, cliquez sur le bouton **Cancel** (7).
