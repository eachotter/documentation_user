.. _doc_mobile_getting_stated:

Installation
------------

Pour installer l’application EachOtter sur votre téléphone iOS ou Android, vous devrez vous rendre sur le store correspondant
à votre système d’exploitation et rechercher l’application « EachOtter ».
L’installation se fera automatiquement dès que vous aurez appuyé sur le bouton « installer ».


Mise à Jour
-----------

La mise à jour de l’application se fera depuis le store correspondant à votre système d’exploitation.
Vous serez alerté via une notification de votre store lorsqu’une mise à jour est disponible.


Désinstallation
---------------

Pour désinstaller l’application EachOtter de votre téléphone,
il suffit de vous rendre dans la liste des applications installées sur votre téléphone portable,
de cliquer sur « EachOtter » et de cliquer sur le bouton « Désinstaller ».
