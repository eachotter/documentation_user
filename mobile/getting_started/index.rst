Pour Commencer
==============

.. toctree::
   :maxdepth: 1
   :name: toc-getting-started

   getting_started

.. history
.. authors
.. license
