A propos de l'application Mobile
================================

Voici la documentation de la partie Mobile qui est l'application client de EachOtter.

L’application mobile EachOtter vous permet d'accéder aux services de EachOtter tel que la commande, la messagerie ou bien encore le vote de musique.


.. history
.. authors
.. license
