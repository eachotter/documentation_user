.. EachOtter User documentation master file, created by
   sphinx-quickstart on Sat Dec 12 16:57:47 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

EachOtter Documentation Utilisateur
===================================

.. attention:: Cette documentation est en perpetuelle évolution.

               Merci de vous informer d'éventuelle modification.

Bienvenue sur la documentation utilisateur de EachOtter.

La Table des Matières sur la version PDF et la barre de navigation sur la version `en ligne <https://eachotter.readthedocs.io/>`_ vous permet d'accéder
rapidement au sujet de la documentation qui vous interesse. Vous pouvez aussi chercher
via la barre de recherche en haut à gauche sur la version `en ligne <https://eachotter.readthedocs.io/>`_ de la documenation.


.. toctree::
   :maxdepth: 1
   :caption: General
   :name: sec-general

   about/index

.. toctree::
   :maxdepth: 1
   :caption: Web Application
   :name: sec-webapp

   webapp/about/index
   webapp/getting_started/index
   webapp/functionalities/index

.. toctree::
   :maxdepth: 1
   :caption: Application Mobile
   :name: sec-mobile

   mobile/about/index
   mobile/getting_started/index
   mobile/functionalities/index


.. Indices and tables
.. ------------------
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
